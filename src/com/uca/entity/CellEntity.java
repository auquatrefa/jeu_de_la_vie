package com.uca.entity;

import java.lang.*;
import java.util.*;

/**
 * Represente une cellule de coordonné x et y
*/
public class CellEntity {
    private final int x;
    private final int y;

    public CellEntity(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public String toString() {
        return x+","+y;
    }

    // redéfinition des méthodes equals et hashCode pour pouvoir comparer deux cellules entre elles
    // (utile pour contains())
    @Override
    public boolean equals(Object other) {
        CellEntity o = (CellEntity) other;
        return (this.x == o.getX() && this.y == o.getY());
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
