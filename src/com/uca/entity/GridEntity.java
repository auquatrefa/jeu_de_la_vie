package com.uca.entity;

import java.util.List;
import java.util.ArrayList;
import java.sql.*;

/**
 * Représente l'état de la grille
 * Toutes les cellules de l'état de la grille sont vivantes
 */
public class GridEntity {
    // état de la grille : liste des cellules vivantes
    private final List<CellEntity> state;
    private Connection connection;

    public GridEntity(Connection c) {
        state = new ArrayList<>();
        this.connection = c;
    }

    public GridEntity(List<CellEntity> state, Connection c) {
        this.state = state;
        this.connection = c;
    }

    /**
     * retourne la grille
     */
    public List<CellEntity> getCells() {
        return this.state;
    }

    /**
     * ajoute une cellule vivante à la grille 
     */
    public void addCell(CellEntity c) {
        this.state.add(c);
    }

    /**
     * supprime une cellule vivante de la grille 
     */
    public void removeCell(CellEntity c){
        this.state.remove(c);
    }
}
