package com.uca.dao;

import java.sql.*;
import com.uca.entity.*;
import java.util.List;
import java.util.ArrayList;

public class GridDAO {
    // méthode permettant de récupérer la liste des cellules vivantes
    public List<CellEntity> getGrid(Connection connect){
        List<CellEntity> state = new ArrayList<>();

        try{
            PreparedStatement preparedStatement = connect.prepareStatement(
                "SELECT x,y FROM plateau WHERE etat = true;"
            );
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                int x = resultSet.getInt("x");
                int y = resultSet.getInt("y");
                CellEntity cellule = new CellEntity(x,y);
                state.add(cellule);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return state;
    }

    // méthode permettant d'obtenir l'état de la cellule passée en paramètre
    private boolean getState(CellEntity cell, Connection connect) {
        boolean etat;
        if (this.getGrid(connect).contains(cell)){
            etat = true;
        }
        else {
            etat = false;
        }
        return etat;
    }

    // inverse l'état d'une cellule lorsque celle-ci est cliquée
    // permet de faire apparaitre/disparaitre les points rouges
    public void changeState(CellEntity selectedCell, Connection connect){
        try{
            if(this.getState(selectedCell, connect) == true){
                PreparedStatement preparedStatement = connect.prepareStatement(
                "UPDATE plateau SET etat=false WHERE x = ? AND y = ?;"
                );
                preparedStatement.setInt(1, selectedCell.getX());
                preparedStatement.setInt(2, selectedCell.getY());
                preparedStatement.executeUpdate();

            }else{
                PreparedStatement st = connect.prepareStatement(
                "SELECT etat FROM plateau WHERE x = ? AND y = ?;"
                );
                st.setInt(1, selectedCell.getX());
                st.setInt(2, selectedCell.getY());
                ResultSet resultSet = st.executeQuery();
                while(resultSet.next()){
                    boolean etat = resultSet.getBoolean("etat");
                }

                PreparedStatement preparedStatement = connect.prepareStatement(
                "UPDATE plateau SET etat=true WHERE x = ? AND y = ?;"
                );
                preparedStatement.setInt(1, selectedCell.getX());
                preparedStatement.setInt(2, selectedCell.getY());
                preparedStatement.executeUpdate();
            }
            
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    // passe l'état de chaque cellule à false dans la base de données, permettant de vider la grille
    public void vider(Connection connect){
        try{
            PreparedStatement preparedStatement = connect.prepareStatement(
            "UPDATE plateau SET etat=false;"
            );
            preparedStatement.executeUpdate();
            
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    // méthode prend en entrée une liste de cellules vivantes. Réinitialise la base de données en mettant toutes les cellules à false
    // Enfin passe les cellules données par la liste à true. Cela permet d'initialiser la grille à un état souhaité
    public void changeGrid(Connection connect, List<CellEntity> state){
        try{
            PreparedStatement preparedStatement = connect.prepareStatement(
            "UPDATE plateau SET etat=false;"
            );
            preparedStatement.executeUpdate();

            for(CellEntity cell : state){
                preparedStatement = connect.prepareStatement(
                    "UPDATE plateau SET etat=true WHERE x=? and y=?;"
                );
                preparedStatement.setInt(1, cell.getX());
                preparedStatement.setInt(2, cell.getY());
                preparedStatement.executeUpdate();
            }         
        }catch (SQLException e){
            e.printStackTrace();
        }
    }  

    // méthode permettant de simuler chaque étape du jeu de la vie
    // génère d'abord un tableau 2D contenant pour chaque cellule son nombre de voisins vivants
    // puis modifie la base de données selon les règles du jeu de la vie
    public void suivant(Connection connection){
        int[][] voisins = new int[100][100];
        boolean etat = false;
        PreparedStatement preparedStatement;
        try{
            for(int i=0; i<100; i++){
                for(int j=0; j<100; j++){
                    preparedStatement = connection.prepareStatement(
                        "SELECT etat FROM plateau WHERE x=? and y=?;"
                    );
                    preparedStatement.setInt(1, i);
                    preparedStatement.setInt(2, j);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while(resultSet.next()){
                        etat = resultSet.getBoolean("etat");
                    }
                    if(etat == true){
                        if(i == 0){
                            if(j == 0) { // si coin haut gauche
                                voisins[i][j+1] += 1;
                                voisins[i+1][j+1] += 1;
                                voisins[i+1][j] += 1;
                            }
                            else if(j == 99) { // si coin haut droit
                                voisins[i][j-1] += 1;
                                voisins[i+1][j-1] += 1;
                                voisins[i+1][j] += 1;
                            } else { // si ligne haut
                                voisins[i][j-1] += 1;
                                voisins[i+1][j-1] += 1;
                                voisins[i+1][j] += 1;
                                voisins[i+1][j+1] += 1;
                                voisins[i][j+1] += 1;
                            }

                        } else if (i == 99) { 
                            if(j == 0) { // si coin bas gauche
                                voisins[i-1][j] += 1;
                                voisins[i-1][j+1] += 1;
                                voisins[i][j+1] += 1;
                            }
                            else if(j == 99) { // si coin bas droit
                                voisins[i][j-1] += 1;
                                voisins[i-1][j-1] += 1;
                                voisins[i-1][j] += 1;
                            } else { // si ligne bas
                                voisins[i][j-1] += 1;
                                voisins[i-1][j-1] += 1;
                                voisins[i-1][j] += 1;
                                voisins[i-1][j+1] += 1;
                                voisins[i][j+1] += 1;
                            }
                            
                        }
                        else if (j == 0) { // si colonne de gauche
                            voisins[i-1][j] += 1;
                            voisins[i-1][j+1] += 1;
                            voisins[i][j+1] += 1;
                            voisins[i+1][j+1] += 1;
                            voisins[i+1][j] += 1;
                        } else if (j == 99) { // si colonne de droite
                            voisins[i-1][j] += 1;
                            voisins[i-1][j-1] += 1;
                            voisins[i][j-1] += 1;
                            voisins[i+1][j-1] += 1;
                            voisins[i+1][j] += 1;
                        } else { // sinon
                            voisins[i-1][j] += 1;
                            voisins[i-1][j+1] += 1;
                            voisins[i][j+1] += 1;
                            voisins[i+1][j+1] += 1;
                            voisins[i+1][j] += 1;
                            voisins[i+1][j-1] += 1;
                            voisins[i][j-1] += 1;
                            voisins[i-1][j-1] += 1;
                        }
                    }
                }
            }

            for(int x=0; x<100; x++){
                for(int y=0; y<100; y++){
                    preparedStatement = connection.prepareStatement(
                        "SELECT etat FROM plateau WHERE x=? and y=?;"
                    );
                    preparedStatement.setInt(1, x);
                    preparedStatement.setInt(2, y);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while(resultSet.next()){
                        etat = resultSet.getBoolean("etat");
                    }
                    if(etat == true && !(voisins[x][y] == 2 || voisins[x][y] == 3)){
                        preparedStatement = connection.prepareStatement(
                        "UPDATE plateau SET etat=false WHERE x=? and y=?;"
                        );
                        preparedStatement.setInt(1, x);
                        preparedStatement.setInt(2, y);
                        preparedStatement.executeUpdate();
                    }else if(etat == false && voisins[x][y] == 3){
                        preparedStatement = connection.prepareStatement(
                        "UPDATE plateau SET etat=true WHERE x=? and y=?;"
                        );
                        preparedStatement.setInt(1, x);
                        preparedStatement.setInt(2, y);
                        preparedStatement.executeUpdate();
                    }
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}