package com.uca.dao;

import java.sql.*;

public class _Initializer {
    // nom de la table contenant la grille
    final static String TABLE = "plateau";
    // taille de grille
    final static int SIZE = 1000;

    /**
     * cette méthode permet d'initialiser en créant une table pour la grille si elle n'existe pas
     */
    public static void Init(){
        Connection connection = _Connector.getMainConnection();

        try{       
            if (!tableExists(connection, "plateau")){   
                PreparedStatement statement;
                statement = connection.prepareStatement(
                    "CREATE TABLE plateau(x int NOT NULL, y int NOT NULL, etat boolean NOT NULL, PRIMARY KEY(x,y));"
                    );
                statement.executeUpdate();

                for(int i=0; i<100; i++){
                    for(int j=0; j<100; j++){
                        PreparedStatement preparedStatement = connection.prepareStatement(
                                "INSERT INTO plateau (x, y, etat) VALUES (?, ?, false);"
                            );
                        preparedStatement.setInt(1, i);
                        preparedStatement.setInt(2, j);
                        preparedStatement.executeUpdate();
                    }
                }
            }

            connection.commit();
        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    /**
     * teste si une table existe dans la base de données 
     */
    private static boolean tableExists(Connection connection, String tableName) throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        ResultSet resultSet = meta.getTables(null, null, tableName, new String[]{"TABLE"});
        return resultSet.next();
    }

    // fonction effectuant un rollback sur la session donnée permettant annuler modifications
    public static void annule(Connection connection){
        try{
            connection.rollback();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    // fonction effectuant un commit sur la session donnée permettant valider modifications
    public static void valide(Connection connection){
        try{
            connection.commit();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
