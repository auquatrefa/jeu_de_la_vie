package com.uca.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class _Connector {

    private static String url = "jdbc:postgresql://localhost/life";
    private static String user = "auquatrefa";
    private static String passwd = "coucou";

    private static Connection connect;

    static final Map<Integer, Connection> connections = new HashMap<>();

    // méthode prend en paramètre l'identifiant d'un utilisateur renvoie la connection associée dans la HashMap
    // répertoriant les sessions. Si l'id est null, créer une Connection qu'il associe à id et renvoie.
    public static Connection getConnection(Integer id){
        if(connections.get(id) == null){
            connections.put(id, _Connector.getNewConnection());
        }
        Connection c = connections.get(id);
        return connections.get(id);
    }

    // méthode retournant la connection de la session actuelle
    public static Connection getMainConnection(){
        if(connect == null){
            connect = getNewConnection();
        }
        return connect;
    }

    // méthode qui créer une nouvelle connection et la renvoie
    private static Connection getNewConnection() {
        Connection c;
        try {
            c = DriverManager.getConnection(url, user, passwd);
            // pour que l'auto commit soit désactivé sur toutes les connections
            c.setAutoCommit(false); 
            // transactions en read committed pour empecher les lectures impropres
            c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED); 
        } catch (SQLException e) {
            System.err.println("Erreur en ouvrant une nouvelle connection.");
            throw new RuntimeException(e);
        }
        return c;
    }
}
