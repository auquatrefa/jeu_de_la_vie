package com.uca;

import com.uca.dao.*;
import com.uca.gui.*;

import com.uca.core.*;
import com.uca.entity.*;

import com.google.gson.Gson;

import static spark.Spark.*;
import spark.*;

import java.util.*;

public class StartServer {

    public static void main(String[] args) {
        //Configuration de Spark
        staticFiles.location("/static/");
        port(8081);

        // Création de la base de données, si besoin
        _Initializer.Init();

        /**
         * Définition des routes
         */

        // index de l'application
        get("/", (req, res) -> {
                return IndexGUI.getIndex();
            });

        // retourne l'état de la grille
        get("/grid", (req, res) -> {
                res.type("application/json");
                return new Gson().toJson(GridCore.getGrid(getSession(req)));
            });

        // inverse l'état d'une cellule 
        put("/grid/change", (req, res) -> {
                Gson gson = new Gson();
                CellEntity selectedCell = (CellEntity) gson.fromJson(req.body(), CellEntity.class);
                int numSession = getSession(req);
                GridCore.changeState(numSession, selectedCell);
                
                return new Gson().toJson(GridCore.getGrid(numSession));
            });

        // sauvegarde les modifications de la grille 
        post("/grid/save", (req, res) -> {
                int numSession = getSession(req);
                _Initializer.valide(_Connector.getConnection(numSession));
                return "";
            });

        // annule les modifications de la grille 
        post("/grid/cancel", (req, res) -> {
            int numSession = getSession(req);
            _Initializer.annule(_Connector.getConnection(numSession));
            GridEntity plateau = new GridEntity(GridCore.getGrid(getSession(req)), _Connector.getConnection(numSession));
            return new Gson().toJson(plateau.getCells());
        });

        // charge un fichier rle depuis un URL
        put("/grid/rle", (req, res) -> {
                String RLEUrl = req.body();
                int numSession = getSession(req);
                List<CellEntity> state = GridCore.decodeRLE(RLEUrl);
                GridCore.changeGrid( _Connector.getConnection(numSession), state);
                return new Gson().toJson(GridCore.getGrid(getSession(req)));
            });

        // vide la grille
        post("/grid/empty", (req, res) -> {
                int session = getSession(req);
                GridCore.vider(session);
                return "";
            });

        // met à jour la grille en la remplaçant par la génération suivante
        post("/grid/next", (req, res) -> {
                int numSession = getSession(req);
                GridCore.suivant(_Connector.getConnection(numSession));
                return new Gson().toJson(GridCore.getGrid(getSession(req)));
            });

    }

    /**
     * retourne le numéro de session
     * il y a un numéro de session différent pour chaque onglet de navigateur
     * ouvert sur l'application
     */
    public static int getSession(Request req) {
        return Integer.parseInt(req.queryParams("session"));
    }
}
